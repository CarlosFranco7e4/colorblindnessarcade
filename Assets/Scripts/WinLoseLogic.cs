﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class WinLoseLogic : MonoBehaviour
{
    public bool hasEnd = false;
    
    public GameObject CanvasResult;

    public void endGame()
    {
        if (hasEnd == false)
        {
            hasEnd = true;
            Debug.Log("GAME OVER");
            CanvasResult.SetActive(true);
        }

    }
 
}
