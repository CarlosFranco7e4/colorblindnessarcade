﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewBehaviourScript : MonoBehaviour
{
    // utilizare este script que no vale para nada para meter enlaces de interes
    //https://www.colorlitelens.com/mosaic-test.html
    //https://www.google.com/search?q=gradient+color+line+colorblind&rlz=1C1GCEB_en&tbm=isch&source=iu&ictx=1&fir=VGh8lF7fo4zUrM%252CwWbGJYJuRzfDGM%252C_%253BsMoKejgcov8hUM%252Cla8jLa0lQXJb1M%252C_%253BaiZGrIo5rDgLiM%252CYW-QSM5qxyH5bM%252C_%253BneG0V_gTcMKgZM%252CJQ6UqBf24YRG1M%252C_%253BPsA0xB-gZVFx9M%252CF-rzcOMLo2el9M%252C_%253BB7SEaXP8vNpN-M%252CwWbGJYJuRzfDGM%252C_%253BHaGmlq9JUXxEAM%252CQ7RBXdsy02gLsM%252C_%253B_CIBr0HZBUW4EM%252CYW-QSM5qxyH5bM%252C_%253BayMPHvXas2wjiM%252C_2lbI1Nh3YsroM%252C_%253B8x_JAkFf5x6dXM%252Cn4GKouNCXICr4M%252C_&vet=1&usg=AI4_-kTS5pOAYybzlvFZaZAWc8bkbrf2Cw&sa=X&ved=2ahUKEwitxcDct_zzAhUKjhQKHbfqCsMQ9QF6BAgUEAE#imgrc=B7SEaXP8vNpN-M
    
    //para trabajar con las logicas de color dividir Hue de GIMP entre 360, ej: 300/360= H:0.83, 100/100= S: 1, V 57.3/100 = 0.57
    
    //LVL0:
    //BLACK/WHITE
    
    //LVL1:
    //GREEN/RED:
    //GREEN
    
    //MAX: H: 0.308 S: 0.876 V: 0.57
    //MIN H: 0.308 S: 0.868 V: 0.15
    
    //RED
    
    //MAX: H: 0 S: 1 V: 0.73
    //MIN: H: 0 S: 1 v:0.38
    
    //LVL2:
    //PURPLE/BLUE:
    //PURPLE
    
    //MAX: H:0.83 S:1 V:0.57
    
    //MIN: H: 0.83 S: 1 V: 0.157
    
    //BLUE
    
    //MAX: H: 0.66 S: 1 V: 0.79
    //MIN: H:0.66 S: 1 V: 0.39
    
    //LVL3:
    //PURPLE/CYAN:
    //PURPLE
    
    //MAX: H: 0.897 S: 0.93 V: 0.847
    //MIN: H: 0.898 S: 0.93 V: 0.455
    
    //CYAN
    //MAX: H: 0.48 S: 0.538 V: 0.408
    //MIN: H: 0.48 S: 0.54 V: 0.23
}
