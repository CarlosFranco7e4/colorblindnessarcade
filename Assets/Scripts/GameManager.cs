﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public Stage stage;
    private Level[] levels;
    public int currentLevel;
    public float oneTry = 0;
    public static GameManager instance;

    public int currentScore;
    public int resultStagePercent;

    public int scoreProtanopia;
    public int scoreDeuteranopia;
    public int scoreTritanopia;

    public Text levelText;
    public Text diagnosticText;


    public Text resultProtonopia;
    public Text resultDeuteranopia;
    public Text resultTritanopia;


    public GameObject resultScreen;
    public GameObject levelScreen;
    public GameObject resultNormalVision;
    public GameObject resultProtoanomalia;
    public GameObject resultDeuteranomalia;
    public GameObject resultTritanomalia;
    public GameObject resultUnknow;



    public Text percentAccurateText;
    private static GameManager _instance;
    public GameObject blindGrid;

    public static GameManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<GameManager>();
            }

            return _instance;
        }
    }

    void Awake()
    {
        levels = gameObject.GetComponents<Level>();
        //DontDestroyOnLoad(gameObject);
    }
    void Start()
    {
        blindGrid = GameObject.FindGameObjectWithTag("BlindGrid");
        levels[currentLevel].StartLevel(true);

        instance = this;
        levelText.text = "Level 0: ";

    }



    /*
    void Update()
    {
        if (stage == 20 && !resultScreen.activeInHierarchy)
        {

        }
    }
*/
    public void ClickedCorrectCell()
    {
        levels[currentLevel].nextStage();
        //añadir text en canvas para poder trabajar aqui con los stages
        //añadir clase con formula 1*100/nStages=stageProgression
        percentAccurateText.text = "Accurate: " + resultStagePercent + "%";
        resultStagePercent += 5;

        if(currentLevel == 1)
        {
            scoreProtanopia = resultStagePercent;
        }
        else if(currentLevel == 2)
        {
            scoreDeuteranopia = resultStagePercent;
        }
        else if(currentLevel == 3)
        {
            scoreTritanopia = resultStagePercent;
        }
        

    }
    public void ClickedWrongCell()
    {
        oneTry++;
        if(oneTry == 2)
        {
            NextLevel();
            oneTry = 0;
            //acaba la partida
        }
    }
    public void NextLevel()
    {
        if (currentLevel < levels.Length-1)
        {
            currentLevel++;

            resultStagePercent = 0;
            percentAccurateText.text = "Accurate: 0%";



            levelText.text = "Level " + currentLevel + ": ";

            levels[currentLevel].StartLevel();
            if(currentLevel == 1)
            {
                diagnosticText.text = "Protonopia";

            }
            else if (currentLevel == 2)
            {
                diagnosticText.text = "Deuteranopia";
            }
            else 
            {
                diagnosticText.text = "Tritanopia";
            }
        }
        else
        {
            blindGrid.gameObject.SetActive(false);
            levelScreen.gameObject.SetActive(false);
            resultScreen.gameObject.SetActive(true);

            resultProtonopia.text = "Protonopia: " + scoreProtanopia + "%";
            resultDeuteranopia.text = "Deuteranopia: " + scoreDeuteranopia + "%";
            resultTritanopia.text = "Tritanopia: " + scoreTritanopia + "%";

            if(scoreProtanopia <= 85 && scoreDeuteranopia > 85 && scoreTritanopia >85)
            {
                resultNormalVision.gameObject.SetActive(false);
                resultProtoanomalia.gameObject.SetActive(true);
            }
            else if (scoreProtanopia > 85 && scoreDeuteranopia <= 85 && scoreTritanopia > 85)
            {
                resultNormalVision.gameObject.SetActive(false);
                resultDeuteranomalia.gameObject.SetActive(true);
            }
            else if (scoreProtanopia > 85 && scoreDeuteranopia > 85 && scoreTritanopia <= 85)
            {
                resultNormalVision.gameObject.SetActive(false);
                resultTritanomalia.gameObject.SetActive(true);
            }
            else
            {
                resultNormalVision.gameObject.SetActive(false);
                resultUnknow.gameObject.SetActive(true);
            }

            Debug.Log("GAME OVER");
            //añadir metodo para abrir canvas con todos los textos con los resultados almacenados durante la ejecucion

        }

        /*blindGrid.gameObject.SetActive(false);
        Debug.Log("PERFECT");
        //acaba la partida*/


    }
}
