﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlindGrid : MonoBehaviour
{
    public GameObject cellPrefab;
    GameObject[,] objectCells;
    public int height = 9;
    public int width = 9;
    public float cellSize = 0.5f;
    public ColorVariance colorVariance;
    public ColorVariance initialColor;
    public ColorVariance correctAreaColor;
    BlindCell[,] cells;
    // Start is called before the first frame update

    void Awake()
    {

    }

    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
    }

    public void InitializeGrid()
    {
        correctAreaColor = initialColor;
        objectCells = new GameObject[width, height];
        cells = new BlindCell[width, height];
        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                objectCells[x, y] = Instantiate(cellPrefab, new Vector3(x * cellSize + cellSize / 2.0f, y * cellSize + cellSize / 2.0f, 0), Quaternion.identity);
                objectCells[x, y].transform.parent = transform;
                cells[x, y] = objectCells[x, y].GetComponent<BlindCell>();
                cells[x, y].SetSize(cellSize);
            }
        }
        CenterGrid();

    }


    public IEnumerator AnimateGrid()
    {
        while (true)
        {
            yield return new WaitForSeconds(1f);
            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    if (cells[x, y].cellCorrect)
                    {
                        cells[x, y].SetColor(ColorTools.RandomColorVariance(correctAreaColor));
                    }
                    else
                    {
                        cells[x, y].SetColor(ColorTools.RandomColorVariance(colorVariance));
                    }
                }
            }
        }
    }

    //TODO. This is an example.
    public void SetGridColors() {
        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                cells[x, y].SetColor(ColorTools.RandomColorVariance(colorVariance));
            }
        }

    }

    public void RefreshGrid()
    {
        SetGridColors();
        RandomizeCorrectArea();
    }

    void RandomizeCorrectArea()
    {
        System.Random rnd = new System.Random();
        SetCorrectArea(rnd.Next(1, width - 1), rnd.Next(1, width - 1));
    }

    //Sets the area of correct clickable cells. Not meant to be called directly.
    void SetCorrectArea(int x, int y)
    {
        //reset cells
        for (int i = 0; i < width; i++)
        {
            for (int j = 0; j < height; j++)
            {
                cells[i, j].cellCorrect = false;
            }
        }
        //set correct cells
        if (x > 0 && y > 0 && x < height-1 && y < width-1)
        {
            for(int i = x-1; i <= x+1; i++)
            {
                for (int j = y-1; j <= y + 1; j++)
                {
                    cells[i, j].cellCorrect = true;
                    cells[i, j].SetColor(ColorTools.RandomColorVariance(correctAreaColor));
                }
            }
        }
    }
    public void SetCorrectAreaColor(ColorVariance cv)
    {
        correctAreaColor = cv;
        Debug.Log(cv.baseHue);

    }

    //Modifies the grid position to visually center it (relative to the current position). Should only be called once. Meant to make positioning on the editor easier.
    void CenterGrid()
    {
        gameObject.transform.position = transform.position - (new Vector3(width * cellSize / 2.0f, height * cellSize / 2.0f));
    }
}
