﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct ColorVariance
{
    public float baseHue;
    public float hueVariance;
    public float baseSaturation;
    public float saturationVariance;
    public float baseValue;
    public float valueVariance;

    public ColorVariance(float baseHue, float hueVariance, float baseSaturation, float saturationVariance, float baseValue, float valueVariance)
    {
        this.baseHue = baseHue;
        this.hueVariance = hueVariance;
        this.baseSaturation = baseSaturation;
        this.saturationVariance = saturationVariance;
        this.baseValue = baseValue;
        this.valueVariance = valueVariance;
    }
    public static ColorVariance operator -(ColorVariance a)
    {
        return new ColorVariance(-a.baseHue, -a.hueVariance, -a.baseSaturation, -a.saturationVariance, -a.baseValue, -a.valueVariance);
    }
    public static ColorVariance operator +(ColorVariance a, ColorVariance b)
    {
        return new ColorVariance(a.baseHue + b.baseHue, a.hueVariance + b.hueVariance, a.baseSaturation + b.baseSaturation, a.saturationVariance + b.saturationVariance, a.baseValue + b.baseValue, a.valueVariance + b.valueVariance);
    }
    public static ColorVariance operator -(ColorVariance a, ColorVariance b)
    {
        return a + (-b);
    }
    public static ColorVariance operator *(ColorVariance a, float b)
    {
        return new ColorVariance(a.baseHue*b, a.hueVariance*b, a.baseSaturation*b, a.saturationVariance*b, a.baseValue*b, a.valueVariance*b);
    }
    public static ColorVariance operator /(ColorVariance a, float b)
    {
        return a * (1 / b);
    }
}

public class ColorTools
{
    // Variance is a percentage. The max deviation from the base value
    public static Color RandomColorVariance(float baseHue, float hueVariance, float baseSaturation, float saturationVariance, float baseValue, float valueVariance)
    {
        return RandomColor(baseHue * (1f - (hueVariance * 0.01f)), baseHue * (1f + (hueVariance * 0.01f)),
            baseSaturation * (1f - (saturationVariance * 0.01f)), baseSaturation * (1f + (saturationVariance * 0.01f)),
            baseValue * (1f - (valueVariance * 0.01f)), baseValue * (1f + (valueVariance * 0.01f)));
    }

    public static Color RandomColorVariance(ColorVariance cv)
    {
        return RandomColorVariance(cv.baseHue, cv.hueVariance, cv.baseSaturation, cv.saturationVariance, cv.baseValue, cv.valueVariance);
    }

    public static Color RandomColor(float minHue, float maxHue, float minSaturation, float maxSaturation, float minValue, float maxValue)
    {
        float hue = Random.Range(minHue, maxHue);
        float saturation = Random.Range(minSaturation, maxSaturation);
        float value = Random.Range(minValue, maxValue);
        return Color.HSVToRGB(hue, saturation, value);
    }

    public static ColorVariance InterpolateColor(ColorVariance color, ColorVariance targetColor, float percentage)
    {
        ColorVariance newColor;
        newColor = color + (targetColor - color) * percentage;
        Debug.Log(newColor.baseHue);
        return newColor;
    }
}
