﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Level : MonoBehaviour
{
    public ColorVariance colorVariance;
    public ColorVariance initialColor;
    [SerializeField]
    private int progression = 0;
    public BlindGrid blindGrid;
    public int nStages = 20;
    public Level()
    {

    }

    public void nextStage()
    {
        //if (progression >= 19)
        if (progression >= nStages-1)
        {
            GameManager.Instance.NextLevel();
        }
        else
        {
            progression++;
            //blindGrid.colorVariance1.baseHue = blindGrid.colorVariance1.baseHue - 0.01f;
            blindGrid.SetCorrectAreaColor(ColorTools.InterpolateColor(initialColor, blindGrid.colorVariance, progression / (float) nStages));
            blindGrid.RefreshGrid();
        }
    }

    // Start is called before the first frame update
    void Awake()
    {
        GameObject gridObject = GameObject.FindWithTag("BlindGrid");
        blindGrid = gridObject.GetComponent<BlindGrid>();

    }
    public void StartLevel(bool firstLevel = false)
    {
        blindGrid.initialColor = initialColor;
        blindGrid.colorVariance = colorVariance;
        blindGrid.SetCorrectAreaColor(ColorTools.InterpolateColor(initialColor, blindGrid.colorVariance, progression / (float)nStages));
        if (firstLevel)
        {
            blindGrid.InitializeGrid();
            StartCoroutine(blindGrid.AnimateGrid());
        }
        blindGrid.RefreshGrid();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
