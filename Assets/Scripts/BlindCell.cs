﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlindCell : MonoBehaviour
{
    BlindGrid parent;
    SpriteRenderer spriteRenderer;
    BoxCollider2D boxCollider;
    public bool cellCorrect;



    // Start is called before the first frame update
    void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        boxCollider = GetComponent<BoxCollider2D>();
        spriteRenderer.sprite = Resources.Load<Sprite>("white");
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void SetColor(Color color)
    {
        spriteRenderer.color = color;
    }

    public void SetSize(float size)
    {
        spriteRenderer.size = new Vector2(size, size);
        boxCollider.size = new Vector2(size, size);
    }

    //Put any code that should execute when the cell is clicked here.
    private void OnMouseDown()
    {
        if (cellCorrect) {
            GameManager.Instance.ClickedCorrectCell();
            Debug.Log("You clicked a correct cell!");
        }
        else
        {
            GameManager.Instance.ClickedWrongCell();
        }
    }
}
