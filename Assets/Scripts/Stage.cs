﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stage : MonoBehaviour
{
    public float progression;

    public BlindCell blindCell;
    public BlindGrid blindGrid;

    ColorVariance colorVariance = new ColorVariance();

    //class that determine if it's correct area then push difficult

    void nextStage()
    {
        for (int i = 0; i <= progression; i ++)
        {
            if (blindCell.cellCorrect)
            {
                progression++;

            }
            if(progression %4 == 0)
            {
                blindGrid.initialColor.baseHue = (float)-0.1;
            }
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        nextStage();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

}
